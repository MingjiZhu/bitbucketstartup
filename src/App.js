import swimWatch from './swimWatch.png';
import deviceScore from './deviceScore.png';
import './App.css';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import {
  CardContent,
  Grid,
} from "@material-ui/core";


const useStyles = makeStyles({
  root: {
    minWidth: 275,
    maxWidth: 545,
  },
  title: {
    fontSize: 14,
  },
});

function App() {
  const classes = useStyles();
  return (
    <div className="App">
      <header className="App-header">
        <Card className={classes.root}>
          <CardContent>
            <Grid container spacing={6}>
              <Grid item xs={12} md={6}>
                <img src={swimWatch} alt="logo" style={{ width: 300 }} />
              </Grid>
              <Grid item xs={12} md={6}>
                <img src={deviceScore} alt="logo" style={{ width: 200 }} />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
        <br />
        <Card className={classes.root}>
          <CardContent>
            <Typography className={classes.title} color="textSecondary" gutterBottom>
              ABOUT GARMIN SWIM
            </Typography>
            <Typography variant="body2" component="p" align="left">
              The Garmin Swim is a fitness watch created exclusively for
              swimmers. Using a highly sensitive internal accelerometer,
              the watch can detect a user's swim stroke automatically,
              track number of laps completed and total distance covered.
              The watch has a sleek design to minimize drag, and a simple
              and intuitive interface, which lets users customize their
              workout easily and read metrics quickly. Features such as
              drill logs and automatic pausing ensure that swimming metrics
              are recorded as accurately as possible. Once the user has completed
              their work out, data can be wirelessly transferred to a number of
              devices through ANT+ technology. Through Garmin's Garmin Connect
              web platform, users can analyze their workouts, set goals,
              create custom workouts, and share results through social media.
          <br />
            </Typography>

          </CardContent>
        </Card>
        <br />
        <Button variant="contained" color="secondary" href="https://vandrico.com/wearables/device/garmin-swim.html">
          More Detail
        </Button>
      </header>
    </div>
  );
}

export default App;
