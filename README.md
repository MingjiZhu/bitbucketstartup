
## Install the app

1. You will see the clone button under the **Source** heading. Click that button.
2. Now click **Copy** button next to the input box.
3. Go to your local and create a file. 
4. Open the directory you just created and right click to paste the clone link.
---

## How to run the app

To avoid heavy file transfer issue, node_modules file has been deleted. \
You need to install npm first. \

Open project in command line and type 

### `npm install` 
、
Runs the app in the development mode using\ 
### `npm start`
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

---

## How build the app

### `npm build` 

Go to the directory and enter above command to build the app

---
## License

License for this project is BSD standard license which you could see in LICENSE.txt file. The reason for using this license is to make this demo project an open source code and allow others to use, redistribute and modify the code. 

